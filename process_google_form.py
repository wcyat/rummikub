import pandas as pd

form_df = pd.read_csv("google_form.csv")

form_df.columns = ["Timestamp", "Email", "Name",
                   "Class", "Class Number", "Special Requests"]

participants_df = form_df[["Class", "Class Number", "Name"]]

for i in ["Present", "First round", "Second round", "Third round", "Total"]:
    participants_df.insert(len(participants_df.columns),
                           i, value=pd.Series(dtype='int'))

participants_df.to_csv("participants.csv", index=False)
